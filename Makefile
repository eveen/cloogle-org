CLM:=clm
BACKEND_LIBS:=\
	-I backend\
	-I backend/Cloogle\
	-I backend/Cloogle/libcloogle\
	-I backend/Cloogle/regex\
	-IL Platform
CLMFLAGS:=$(BACKEND_LIBS) -nr -nt

BACKEND_BIN:=builtin_syntax
BACKEND_TEST:=$(addprefix test_,$(BACKEND_BIN))

test: $(BACKEND_TEST) test_properties

$(BACKEND_TEST): test_%: backend/test/%
	./$<

$(addprefix backend/test/,$(BACKEND_BIN)): .FORCE
	$(CLM) -I $(dir $@) $(CLMFLAGS) $(notdir $@) -o $@

test_properties: .FORCE
	cleantest \
		--hide start \
		--run testproperties \
		$(addprefix -O ,-d backend $(BACKEND_LIBS) -P OutputTestEvents -r)

.FORCE:

.PHONY: test .FORCE
