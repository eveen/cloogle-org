#!/bin/bash
set -e

apt-get update -qq
apt-get install -qq wget jq

wget -q -O/tmp/t.deb https://github.com/joewalnes/websocketd/releases/download/v0.2.12/websocketd-0.2.12_amd64.deb
dpkg -i /tmp/t.deb
rm /tmp/t.deb

apt-get remove -qq wget
apt-get autoremove -qq
