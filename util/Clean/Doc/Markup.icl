implementation module Clean.Doc.Markup

import _SystemArray
import StdBool
import StdChar
import StdClass
import StdInt
import StdString

import Text.HTML

documentationToHTML :: !(String -> HtmlTag) !String -> [HtmlTag]
documentationToHTML link s = walk 0 0 (size s-1) link (moveBackticksOutsideCurlyBraces s)

moveBackticksOutsideCurlyBraces :: !String -> String
moveBackticksOutsideCurlyBraces s = edit (size s-1) {c \\ c <-: s}
where
	edit :: !Int !*String -> *String
	edit -1 s = s
	edit i s
	| s.[i]=='`' && s.[i-1]=='{' && s.[i-2]=='{' = edit (i-3) {s & [i]='{', [i-2]='`'}
	| s.[i]=='}' && s.[i-1]=='}' && s.[i-2]=='`' = edit (i-3) {s & [i]='`', [i-2]='}'}
	| otherwise = edit (i-1) s

walk :: !Int !Int !Int !(String -> HtmlTag) !String -> [HtmlTag]
walk last i end link s
| i>end = [Text (s%(last,end))]
| otherwise = case s.[i] of
	'`'
		| s.[i+1]=='`' && s.[i+2]==' ' -> case find (i+1) end " ``" s of
			?None   -> walk last (i+1) end link s
			?Just r -> [Text (s%(last,i-1)),CodeTag [] (walk (i+3) (i+3) (r-1) link s):walk (r+3) (r+3) end link s]
		| otherwise -> case find (i+1) end "`" s of
			?None   -> walk last (i+1) end link s
			?Just r -> [Text (s%(last,i-1)),CodeTag [] (walk (i+1) (i+1) (r-1) link s):walk (r+1) (r+1) end link s]
	'\n'
		| s.[i+1]=='\n'
			-> [Text (s%(last,i-1)),BrTag []:walk (i+1) (i+1) end link s]
		| s.[i+1]=='`' && s.[i+2]=='`' && s.[i+3]=='`' -> case find (i+4) end "\n" s of
			?None    -> walk last (i+1) end link s
			?Just nl -> case find nl end "\n```" s of
				?None   -> walk last (i+1) end link s
				?Just r -> [Text (s%(last,i-1)),PreTag [] [Text (s%(nl,r-1))]:walk (r+4) (r+4) end link s]
		# li=first_after_whitespace i s
		| s.[li]=='-' || s.[li]=='*'
			-> [Text (s%(last,i-1)),BrTag [],Text {s.[li]}:walk (li+1) (li+1) end link s]
	'*'
		| s.[i+1]=='*'
			| s.[i+2]=='*' -> case find (i+3) end "***" s of
				?None   -> walk last (i+1) end link s
				?Just r -> [Text (s%(last,i-1)),StrongTag [] [EmTag [] (walk (i+3) (i+3) (r-1) link s)]:walk (r+3) (r+3) end link s]
			| otherwise -> case find (i+2) end "**" s of
				?None   -> walk last (i+1) end link s
				?Just r -> [Text (s%(last,i-1)),StrongTag [] (walk (i+2) (i+2) (r-1) link s):walk (r+2) (r+2) end link s]
		| otherwise -> case find (i+1) end "*" s of
			?None   -> walk last (i+1) end link s
			?Just r -> [Text (s%(last,i-1)),EmTag [] (walk (i+1) (i+1) (r-1) link s):walk (r+1) (r+1) end link s]
	'{' | s.[i+1]=='{' -> case find (i+2) end "}}" s of
		?None   -> walk last (i+1) end link s
		?Just r -> [Text (s%(last,i-1)),link (s%(i+2,r-1)):walk (r+2) (r+2) end link s]
	_
		-> walk last (i+1) end link s

find :: !Int !Int !String !String -> Maybe Int
find i end q s
# sq = size q
| i+sq-1>end
	= ?None
| startsWithAt (i+sq-1) (sq-1) q s
	= ?Just i
	= find (i+1) end q s
where
	startsWithAt :: !Int !Int !String !String -> Bool
	startsWithAt _ -1 _ _ = True
	startsWithAt i j q s = s.[i]==q.[j] && startsWithAt (i-1) (j-1) q s

first_after_whitespace :: !Int !String -> Int
first_after_whitespace i s
| i >= size s   = i-1
| isSpace s.[i] = first_after_whitespace (i+1) s
| otherwise     = i
