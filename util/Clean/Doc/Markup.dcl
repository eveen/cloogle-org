definition module Clean.Doc.Markup

from Text.HTML import :: HtmlTag

documentationToHTML :: !(String -> HtmlTag) !String -> [HtmlTag]
