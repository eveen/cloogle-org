module genabcdoc

import StdFunctions
import StdList
import StdOrdList
import StdString

import Clean.Doc.Markup
import Data.Either
from Data.Func import on, `on`
import Data.List
import Text
import Text.HTML

import Cloogle.API
import Cloogle.DB
import Builtin.ABC

Start = toString (documentation builtin_abc_instructions)

documentation :: ![Either String ABCInstructionEntry] -> HtmlTag
documentation instrs = HtmlTag []
	[ HeadTag []
		[ TitleTag [] [Text "ABC documentation"]
		, StyleTag [TypeAttr "text/css"] [Text ".instruction{border:1px solid black;margin:.5em;padding:.5em;}"]
		]
	, BodyTag []
		[ H1Tag [] [Text "ABC documentation"]
		: concatMap entry instrs ++
		  [H2Tag [] [Text "Index"], index instrs]
		]
	]

index :: ![Either String ABCInstructionEntry] -> HtmlTag
index instrs = UlTag []
	[LiTag [] [ATag [HrefAttr ("#"+++i.aie_instruction)] [Text i.aie_instruction]] \\ i <- sorted]
where
	sorted = sortBy ((<) `on` \e->e.aie_instruction) [i \\ Right i <- instrs]

entry :: !(Either String ABCInstructionEntry) -> [HtmlTag]
entry (Left title) = [H2Tag [] [Text title]]
entry (Right aie)
| aie.aie_description=="There is no documentation for this ABC instruction yet."
	= [CodeTag [] [Text aie.aie_instruction], Text " "]
	= [DivTag [IdAttr aie.aie_instruction, ClassAttr "instruction"]
		[ CodeTag [] (intersperse (Text " ") [Text aie.aie_instruction:[EmTag [] [Text (toString arg)] \\ arg <- aie.aie_arguments]])
		, DivTag [] (documentationToHTML link aie.aie_description)
		]]
where
	link :: !String -> HtmlTag
	link s = ATag [HrefAttr ("#"+++s)] [Text s]
