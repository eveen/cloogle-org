#!/bin/bash
set -e

if [ ! -e libs.json ]; then
   echo "libs.json not found"
	exit -1
fi

DEST="$1"

CURL="curl -sSL --retry 20 --retry-connrefused"

rm -rf "$DEST"
mkdir -p "$DEST"

exec 5< <(jq '.[]' < libs.json | jq '.[]' | jq -cMr '.name,.fetch_url[0,1],.fetch_url[2][0,1],.path')

while read lib <&5
do
	read fetch_method <&5
	read fetch_url <&5
	read platform <&5
	read distribution <&5
	read path <&5

	rm -rf "$DEST/$lib"

	case "$fetch_method" in
		"SVN")
			if [[ "$path" == "null" ]]; then
				echo "Fetching $fetch_url..."
				svn checkout -q "$fetch_url" "$DEST/$lib"
			else
				echo "Fetching $fetch_url/$path..."
				svn checkout -q "$fetch_url/$path" "$DEST/$lib"
			fi
			;;
		"Git")
			if [[ "$path" == "null" ]]; then
				echo "Fetching $fetch_url..."
				git clone -q "$fetch_url" "$DEST/$lib"
			else
				echo "Fetching $fetch_url/$path..."
				git clone -q "$fetch_url" "/tmp/$lib"
				mv "/tmp/$lib/$path" "$DEST/$lib"
				rm -rf "/tmp/$lib"
			fi
			;;
		"CleanDistribution")
			if [[ "$platform" == windows* ]]; then
				URL="https://ftp.cs.ru.nl/Clean/builds/$platform/clean-$distribution-$platform-latest.zip"
				$CURL "$URL" -o temp.zip
				unzip -q -d "$DEST" temp.zip "clean-$distribution/Libraries/*"
				rm temp.zip
				mv "$DEST/clean-$distribution/Libraries"/* "$DEST"
				rmdir "$DEST/clean-$distribution/Libraries"
				rmdir "$DEST/clean-$distribution"
			else
				URL="https://ftp.cs.ru.nl/Clean/builds/$platform/clean-$distribution-$platform-latest.tgz"
				echo "Fetching $URL..."
				$CURL "$URL" | tar xz --exclude=exe -C "$DEST" --strip-components=2 "clean-$distribution/lib"
			fi
			;;
		*)
			echo "Unknown fetch method '$fetch_method'"
			exit 1
			;;
	esac
done

shopt -s globstar
for f in "$DEST"/**/*.[id]cl; do
	enc="$(file -bi "$f" | grep -Po '(?<=charset=).*')"
	if [ "$enc" != 'us-ascii' -a "$enc" != 'binary' -a "$enc" != 'utf-8' ]; then
		iconv -f "$enc" -t utf-8 < "$f" > "$f.tmp"
		mv "$f.tmp" "$f"
		echo "converted $f from $enc to utf-8"
	fi
done
