var share_button;

function restoreShareUI() {
	share_button.disabled = false;
	share_button.type = 'button';
	share_button.value = 'Share';
}

function shareButtonClick() {
	if (share_button.value != 'Share')
		return;

	share_button.disabled = true;
	var url = window.location.pathname.substring(1) + window.location.search + window.location.hash;
	shortenURL('cloogle', url, function (type, msg) {
		share_button.value = msg;
		if (type == 'success') {
			share_button.type = 'text';
			share_button.disabled = false;
			share_button.select();
		}
	});
}

function selectLines(n) {
	var firstline = null;
	browser.state.lines = n;

	var hll = document.getElementsByClassName('hll');
	while (hll.length > 0)
		hll[0].classList.remove('hll', 'hll-first', 'hll-last');

	if (!isNaN(n)) {
		firstline = n;
		var linespan = document.getElementById('line-' + n);
		if (linespan != null)
			linespan.classList.add('hll', 'hll-first', 'hll-last');
	} else if (typeof n == 'string') {
		var match = n.match(/^(\d+)-(\d+)$/);
		if (match != null) {
			firstline = match[1];
			var first = true;
			for (var i = firstline; i < match[2]; i++) {
				var linespan = document.getElementById('line-' + i);
				if (linespan != null) {
					linespan.classList.add('hll');
					if (first) {
						linespan.classList.add('hll-first');
						first = false;
					}
				}
			}
			var linespan = document.getElementById('line-' + match[2]);
			if (linespan != null)
				linespan.classList.add('hll', 'hll-last');
		}
	}

	browser.triggerChange(false);

	return firstline;
}

function selectLinesByEvent(e, n) {
	if (e.shiftKey) {
		if (!isNaN(browser.state.lines)) {
			if (browser.state.lines < n)
				selectLines(browser.state.lines + '-' + n);
			else
				selectLines(n + '-' + browser.state.lines);
			return;
		} else if (typeof n == 'string') {
			var match = n.match(/^(\d+)-(\d+)$/);
			if (match != null) {
				if (n < match[1])
					selectLines(n + '-' + match[2]);
				else
					selectLines(match[1] + '-' + n);
				return;
			}
		}
	}

	selectLines(n);
}

var tableLineNo = null;
function tableHighlightCallback(span, cls, str) {
	var html = '';
	if (tableLineNo == null) {
		html = '<tr id="line-1"><td onclick="selectLinesByEvent(event,1);">1</td><td>';
		tableLineNo = 1;
	}
	var lines = str.split('\n');
	for (var i = 0; i < lines.length-1; i++) {
		tableLineNo++;
		html += highlightCallback(
				'<span class="' + cls + '">' + escapeHTML(lines[i]) + '</span>',
				cls, lines[i]) +
			'</td></tr><tr id="line-' + tableLineNo + '"><td onclick="selectLinesByEvent(event,' + tableLineNo + ');">'
				+ tableLineNo + '</td><td>';
	}
	html += highlightCallback(
		'<span class="' + cls + '">' + escapeHTML(lines[lines.length-1]) + '</span>',
		cls, lines[lines.length-1]);
	return html;
}

var browser = null;
window.onload = function() {
	share_button = document.getElementById('share-button');
	var viewer = document.getElementById('viewer');
	var icl = document.getElementById('icl');

	browser = document.getElementsByClassName('browser')[0].browser({
		newPath: function (path) {
			this.state.mod = path.join('/');
			this.state.lines = null;
			this.newState();
		},
		newHash: function (hash) {
			var hashelems = hash.split(';');
			var update = this.state.mod != hashelems[0];
			this.state.mod = hashelems[0];
			this.state.icl = false;
			this.state.lines = null;
			for (var i = 1; i < hashelems.length; i++) {
				if (hashelems[i] == 'icl')
					icl.checked = this.state.icl = true;
				else if (hashelems[i].substring(0,5) == 'line=')
					this.state.lines = hashelems[i].substring(5);
			}

			if (this.state.lines != null)
				selectLines(this.state.lines);

			browser.openPath(this.state.mod.split('/'));
			browser.triggerChange(update);
		},
		newState: function () {
			var hash = this.state.mod;
			if (this.state.icl)
				hash += ';icl';
			if (this.state.lines != null)
				hash += ';line=' + this.state.lines;

			browser.setHash(hash);
		},
		getUrl: function () {
			var url = 'src.php?mod=' + this.state.mod;
			if (this.state.icl)
				url += '&icl';
			if (this.state.lines != null)
				url += '&line=' + this.state.lines;
			return url;
		},
		viewer: viewer,
		onLoad: function(state, text) {
			tableLineNo = null;
			viewer.innerHTML = '<table class="source-code">' +
					highlightClean(text, tableHighlightCallback) +
				'</table>';
			viewer.scrollLeft = 0;
			if (state.lines != null) {
				var firstline = selectLines(state.lines);
				if (firstline != null)
					browser.scrollTo(document.getElementById('line-' + firstline));
			} else {
				browser.scrollTo();
			}

			var modparts = state.mod.split('/');
			if (modparts.length > 1) {
				var lib = modparts.shift();
				document.title = modparts.join('.') + ' (' + lib + ') - Cloogle library browser';
			}

			restoreShareUI();
		},
		state: {
			icl: false
		}
	});

	browser.state.icl = icl.checked;
	icl.onchange = function() {
		browser.state.icl = this.checked;
		browser.state.lines = null;
		browser.triggerChange();
	};

	browser.open();
};
