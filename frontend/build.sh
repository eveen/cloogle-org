#!/bin/bash
set -e

apt-get update -qq

# PHP dependencies
docker-php-source extract
docker-php-ext-install sockets mysqli
docker-php-source delete

# Logo; birthday patch
apt-get install -qq imagemagick librsvg2-bin
month="$(date +%m)"
day="$(date +%d)"
if [[ "$month" == "02" ]] && [[ "$day" > "15" ]] && [[ "$day" < "24" ]]; then
	patch < birthday.patch
fi
rsvg-convert -w 400 logo.svg > logo.png
convert -resize 200x logo.png logo.png

# Clean installation with indexed libraries, for /src and /doc
apt-get install -qq subversion ca-certificates git jq unzip
mkdir -p /opt/clean
curl -Ls https://ftp.cs.ru.nl/Clean/builds/linux-x64/clean-base-linux-x64-latest.tgz\
	| tar xz -C /opt/clean --strip-components=1
ln -s ../libs.json
../util/fetch_libs.sh /opt/clean/lib

# Build common problems index
apt-get install -qq python3 ca-certificates git
git clone https://gitlab.science.ru.nl/cloogle/common-problems /tmp/common-problems
cd /tmp/common-problems
./build_index.py
mv common-problems.json /var
cd -

# JavaScript dependencies
mkdir /var/www/clean-highlighter
curl "$(curl https://registry.npmjs.org/clean-highlighter/ | jq -r '.versions[."dist-tags".latest].dist.tarball')"\
	| tar xzv --strip-components=1 --directory=/var/www/clean-highlighter
mkdir /var/www/clean-doc-markup
curl "$(curl https://registry.npmjs.org/clean-doc-markup/ | jq -r '.versions[."dist-tags".latest].dist.tarball')"\
	| tar xzv --strip-components=1 --directory=/var/www/clean-doc-markup

apt-get remove -qq imagemagick librsvg2-bin subversion ca-certificates git jq python3
apt-get autoremove -qq
