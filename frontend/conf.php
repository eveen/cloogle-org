<?php
define('CLOOGLE_DB_HOST', 'db');
define('CLOOGLE_DB_USER', 'cloogle');
define('CLOOGLE_DB_NAME', 'cloogledb');
define('CLOOGLE_DB_PASS', 'cloogle');

define('CLOOGLE_KEEP_STATISTICS', true);

define('E_NORESULTS', 127);
define('E_INVALIDINPUT', 128);
define('E_INVALIDNAME', 129);
define('E_INVALIDTYPE', 130);

define('E_CLOOGLEDOWN', 150);
define('E_ILLEGALMETHOD', 151);
define('E_ILLEGALREQUEST', 152);
define('E_TIMEOUT', 153);
define('E_DOSPROTECT', 154);
define('E_QUERYTOOLONG', 155);

define('DOS_MAX_REQUESTS_PER_SECOND', 3);
