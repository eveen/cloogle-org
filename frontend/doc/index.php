<!DOCTYPE html>
<html lang="en">
<head>
	<title>Documentation browser</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="description" content="Cloogle is a search engine for the Clean programming language"/>
	<meta name="keywords" content="Clean,Clean language,Concurrent Clean,search,functions,search engine,programming language,clean platform,iTasks,cloogle,hoogle"/>
	<link rel="stylesheet" href="../common.css" type="text/css"/>
	<style>
		td, th {
			padding-right: unset;
		}

		a:visited {
			color: blue !important;
		}

		#sidebar {
			min-width: 300px;
		}

		#sidebar h3 {
			background: none;
			font-family: sans-serif;
			font-size: 1.17em;
			font-weight: bold;
			line-height: 22px;
			margin-bottom: 17.55px;
			margin-left: 0;
		}

		#sidebar table a {
			color: inherit !important;
			text-decoration: none;
		}

		#sidebar td {
			white-space: nowrap;
		}

		#viewer {
			padding: 10px;
		}

		#viewer > div:first-child {
			position: relative;
		}
	</style>
</head>
<body class="framelike">
	<div id="sidebar">
		<a href="/"><img id="logo" src="../logo.png" alt="Cloogle logo"/></a>
		<h3>Documentation browser</h3>
		<hr/>
		<?php include_once('contents.php'); ?>
		<hr/>
		An up to date PDF version of the language report can be found in your <a style="padding-left:0;" href="https://clean.cs.ru.nl/Download_Clean" target="_blank">Clean distribution</a>.
		<p>&nbsp;</p>
	</div><div id="viewer">
		<?php include_once('src.php'); ?>
		<p>&nbsp;</p>
	</div>
</body>
</html>
