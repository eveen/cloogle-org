<?php
define('CLEANHOME', '/opt/clean');
error_reporting(0);

$f=fopen (CLEANHOME.'/doc/CleanLanguageReport.html','r');
if (!$f)
	die ('Failed to open language report.');

$found_table=false;
while (($line=fgets ($f))!==false){
	if ($found_table){
		echo str_replace ('#9999FF','transparent',$line);
		if (substr ($line,0,8)=='</table>')
			break;
	} else if (substr ($line,0,6)=='<table'){
		echo $line;
		$found_table=true;
	}
}

fclose ($f);
