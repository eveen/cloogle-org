<?php
define('CLEANHOME', '/opt/clean');
error_reporting(0);

$f=fopen (CLEANHOME.'/doc/CleanLanguageReport.html','r');
if (!$f)
	die ('Failed to open language report.');

$found_body=false;
while (($line=fgets ($f))!==false){
	if ($found_body){
		if (substr ($line,0,7)=='</body>'){
			echo '</div>';
			break;
		}
		echo $line;
	} else if (substr ($line,0,5)=='<body'){
		echo '<div'.substr ($line,5);
		$found_body=true;
	}
}

fclose ($f);
