definition module Builtin.ABC

from Data.Either import :: Either
from Cloogle.DB import :: ABCInstructionEntry

builtin_abc_instructions :: [Either String ABCInstructionEntry]
