definition module Builtin.Syntax

/**
 * @property-bootstrap
 *   import StdEnv
 *   from Data.Func import mapSt
 *   import Data.List
 *   import Text
 *   import Regex
 *   from Cloogle.API import :: SyntaxExample{..}
 *   import Cloogle.DB
 *
 *   // ggen isn't usually used, but required to satisfy constraints
 *   ggen{|SyntaxEntry|} _ = []
 *   derive genShow SyntaxEntry, SyntaxExample, CleanLangReportLocation, ?
 *   genShow{|CompiledRegex|} _ _ _ s = s
 *   gPrint{|SyntaxEntry|} se ps = gPrint{|*|} se.syntax_title ps
 *
 *   example_lines :: !SyntaxEntry -> [String]
 *   example_lines se = concatMap (split "\n")
 *     [replaceSubString "\t" "    " e.example \\ e <- se.syntax_examples]
 *
 * @property-test-generator list: SyntaxEntry
 *   builtin_syntax
 */

from Cloogle.DB import :: SyntaxEntry

/**
 * @property has examples: A.se :: SyntaxEntry:
 *   not (isMember se.syntax_title ["array update", "record update"])
 *     ==> not (isEmpty se.syntax_examples)
 *
 * @property comment alignment (at most two different levels): A.se :: SyntaxEntry:
 *   length (removeDup comment_columns) < 3
 *   where
 *     comment_columns = filter ((<>) -1) [indexOf "//" l \\ l <- example_lines se]
 *
 * @property different comment levels should be far apart: A.se :: SyntaxEntry:
 *   case removeDup comment_columns of
 *     [i1,i2:_] -> abs (i2-i1) >. 3
 *     _         -> prop True
 *   where
 *     comment_columns = filter ((<>) -1) [indexOf "//" l \\ l <- example_lines se]
 *
 * @property spaces around comment markers: A.se :: SyntaxEntry:
 *   isEmpty (filter bad (example_lines se))
 *   where
 *     bad s = case indexOf "//" s of
 *       -1 -> False
 *       i  -> s.[i-1] <> ' ' || s.[i+2] <> ' '
 *
 * @property spaces in list comprehensions: A.se :: SyntaxEntry:
 *   isEmpty (filter bad (example_lines se))
 *   where
 *     bad s
 *     # s = replaceSubString "'\\\\'" "" s // special case for '\\' in basic_values
 *     # (results,_) = mapSt check ["<|-","<-:","<-","\\\\"] s
 *     = or results
 *
 *     check op s
 *     # new = replaceSubString (" " +++ op +++ " ") "" s
 *     = (indexOf op new >= 0, new)
 */
builtin_syntax :: [SyntaxEntry]
