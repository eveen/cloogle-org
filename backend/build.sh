#!/bin/bash
set -ev

./install_clean.sh

PACKAGES="file patch unzip jq z3"
apt-get update -qq
apt-get install -qq $PACKAGES --no-install-recommends

make clean
CLEAN_HOME="$CLEAN_HOME-x64" PATH="$CLEAN_HOME/bin:$CLEAN_HOME/exe:$PATH" make -B builddb
make CloogleServer

if [ ! -f libs.json ]; then ln -s ../libs.json; fi
../util/fetch_libs.sh /opt/clean/lib
./builddb > db.jsonl
./CloogleServer --rank-settings-constraints rank_settings_constraints.json | tee rank_settings.json
