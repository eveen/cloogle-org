#!/bin/bash

CLEANDATE="2020-12-20"
PACKAGES="base lib-dynamics lib-gast lib-platform lib-tcpip test test-properties"

CLEAN_PLATFORM=x64 install_clean.sh "$PACKAGES" && mv $CLEAN_HOME{,-x64}
CLEAN_PLATFORM=x86 install_clean.sh "$PACKAGES" && mv $CLEAN_HOME{,-x86}

ln -s $CLEAN_HOME-x86 $CLEAN_HOME
