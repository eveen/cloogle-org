definition module Util.SimpleTCPServer

from StdOverloaded import class zero, class fromString, class toString
from TCPIP import :: IPAddress, :: Port

:: LogMessage req res sentinfo
	= Connected IPAddress
	| Received req
	| Sent res sentinfo
	| Disconnected

:: Logger req res logst sentinfo
	:== (LogMessage req res sentinfo) (?logst) *World -> *(?logst, *World)

:: Server req res st logst sentinfo =
	{ handler           :: !req -> .(st -> *(*World -> *(res, sentinfo, st, *World)))
	, logger            :: ! ?(Logger req res logst sentinfo)
	, port              :: !Int
	, connect_timeout   :: ! ?Int
	, keepalive_timeout :: ! ?Int
	}

serve :: !(Server req res .st logst sentinfo) .st !*World -> *World | fromString req & toString res
